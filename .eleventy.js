module.exports = function(eleventyConfig){
    eleventyConfig.addPassthroughCopy({
      "./src/_includes/style.css": "./style.css",
    });
    return {
        dir: {
            input: "src",
            output: "public",
        }
    }
}